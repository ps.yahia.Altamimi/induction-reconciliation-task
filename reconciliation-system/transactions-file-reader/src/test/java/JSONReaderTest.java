import com.progressoft.reconciliation.read.readers.JSONListProvider;
import com.progressoft.reconciliation.read.models.TransactionDataModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class JSONReaderTest {
    @Test
    public void canCreate() {
        Path path = Paths.get("../sample-files/input-files/online-banking-transactions.json");
        JSONListProvider jsonReader = new JSONListProvider(path);
    }
    @Test
   public void givenJSONFile_whenReading_thenReturnSortedData() throws IOException {
        Path temp = Files.createTempFile(Paths.get("/tmp"), ".json", ("temp"));
        try (PrintWriter printWriter = new PrintWriter(Files.newOutputStream(temp));) {
            printWriter.write("[\n" +
                    "  {\n" +
                    "    \"date\": \"20/01/2020\",\n" +
                    "    \"reference\": \"TR-47884222201\",\n" +
                    "    \"amount\": \"140.00\",\n" +
                    "    \"currencyCode\": \"USD\",\n" +
                    "    \"purpose\": \"donation\"\n" +
                    "  },\n" +
                    "  {\n" +
                    "    \"date\": \"03/02/2020\",\n" +
                    "    \"reference\": \"TR-47884222205\",\n" +
                    "    \"amount\": \"60.000\",\n" +
                    "    \"currencyCode\": \"JOD\",\n" +
                    "    \"purpose\": \"\"\n" +
                    "  },\n" +
                    "  {\n" +
                    "    \"date\": \"10/02/2020\",\n" +
                    "    \"reference\": \"TR-47884222206\",\n" +
                    "    \"amount\": \"500.00\",\n" +
                    "    \"currencyCode\": \"USD\",\n" +
                    "    \"purpose\": \"general\"\n" +
                    "  },\n" +
                    "  {\n" +
                    "    \"date\": \"22/01/2020\",\n" +
                    "    \"reference\": \"TR-47884222202\",\n" +
                    "    \"amount\": \"30.000\",\n" +
                    "    \"currencyCode\": \"JOD\",\n" +
                    "    \"purpose\": \"donation\"\n" +
                    "  },\n" +
                    "  {\n" +
                    "    \"date\": \"14/02/2020\",\n" +
                    "    \"reference\": \"TR-47884222217\",\n" +
                    "    \"amount\": \"12000.000\",\n" +
                    "    \"currencyCode\": \"JOD\",\n" +
                    "    \"purpose\": \"salary\"\n" +
                    "  },\n" +
                    "  {\n" +
                    "    \"date\": \"25/01/2020\",\n" +
                    "    \"reference\": \"TR-47884222203\",\n" +
                    "    \"amount\": \"5000.000\",\n" +
                    "    \"currencyCode\": \"JOD\",\n" +
                    "    \"purpose\": \"not specified\"\n" +
                    "  },\n" +
                    "  {\n" +
                    "    \"date\": \"12/01/2020\",\n" +
                    "    \"reference\": \"TR-47884222245\",\n" +
                    "    \"amount\": \"420.00\",\n" +
                    "    \"currencyCode\": \"USD\",\n" +
                    "    \"purpose\": \"loan\"\n" +
                    "  }\n" +
                    "]");
            printWriter.flush();
        }
        JSONListProvider jsonReader=new JSONListProvider(temp);
        ArrayList<TransactionDataModel> data = jsonReader.getSortedTransactionsList().getData();
        for (int index = 0; index < data.size()-1; index++) {
            String first = data.get(index).getID();
            String second = data.get(index + 1).getID();
            Assertions.assertTrue(first.compareTo(second)<0);
        }
    }
    }


