import com.progressoft.reconciliation.read.readers.SimpleTransactionsSource;
import com.progressoft.reconciliation.read.models.SimpleDataModel;
import com.progressoft.reconciliation.read.models.TransactionDataModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class SimpleFileTest {

    @Test
    public void givenCSVFile_whenGetSize_thenReturnZero() throws FileNotFoundException {
        Path path = Paths.get("/home/user/git/induction-reconciliation-task/sample-files/input-files/bank-transactions.csv");
        SimpleTransactionsSource simpleFile = new SimpleTransactionsSource();
        int size = simpleFile.getData().size();
        Assertions.assertEquals(0, size);
    }

    @Test
    public void givenFilledCSVFile_whenGetData_thenReturnCorrectData
            () throws FileNotFoundException {
        SimpleTransactionsSource simpleFile = new SimpleTransactionsSource();
        ArrayList<TransactionDataModel> arrayList = new ArrayList<>();
        SimpleDataModel dataModel = new SimpleDataModel("1551", "12/12/2012", "JOD", new BigDecimal(45));
        simpleFile.addModel(dataModel);
        int size = simpleFile.getData().size();
        arrayList.add(dataModel);
        Assertions.assertEquals(1, size);
        Assertions.assertEquals(simpleFile.getData(), arrayList);
        SimpleDataModel dataModel1 = new SimpleDataModel("1555", "12/12/2012", "JOD", new BigDecimal(45));
        simpleFile.addModel(dataModel1);
        size = simpleFile.getData().size();
        Assertions.assertEquals(2, size);
        arrayList.add(dataModel1);
        Assertions.assertEquals(simpleFile.getData(), arrayList);
        simpleFile.sort();
        arrayList.sort((t,t1)->t.getID().compareTo(t1.getID()));
        Assertions.assertEquals(arrayList,simpleFile.getData());

    }


}
