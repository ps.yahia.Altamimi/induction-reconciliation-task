import com.progressoft.reconciliation.read.models.SimpleDataModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class SimpleDataModelTest {
    @Test
    public void canCreate() {
        SimpleDataModel simpleDataModel = new SimpleDataModel("747", "10/10/2012", "JOD", new BigDecimal(1.0));
    }
    @Test
    public void givenNullID_whenConstructing_thenFail() {
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                () -> new SimpleDataModel(null, "5/10/1998", "", new BigDecimal(1.0)));
        Assertions.assertEquals("Null ID", nullPointerException.getMessage());

    }

    @Test
    public void givenNullDate_whenConstructing_thenFail() {
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                () -> new SimpleDataModel("12215", null, "JOD", new BigDecimal(1.0)));
        Assertions.assertEquals("Null Date", nullPointerException.getMessage());

    }

    @Test
    public void givenNullCurrencyCode_whenConstructing_thenFail() {
        NullPointerException nullPointerException = Assertions.assertThrows(NullPointerException.class,
                () -> new SimpleDataModel("12215", "25/10/2005", null, new BigDecimal(1.0)));
        Assertions.assertEquals("Null Currency Code", nullPointerException.getMessage());

    }

    @Test
    public void givenNonPositiveAmount_whenConstructing_thenFail() {
        IllegalArgumentException illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new SimpleDataModel("12215", "25/10/2005", "JOD", new BigDecimal(0)));
        Assertions.assertEquals("Non Positive Amount", illegalArgumentException.getMessage());
        illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new SimpleDataModel("12215", "25/10/2005", "JOD", new BigDecimal(-7)));
        Assertions.assertEquals("Non Positive Amount", illegalArgumentException.getMessage());
    }

    @Test
    public void givenValidValues_whenConstructing_thenGetCorrectValues() {
        SimpleDataModel simpleDataModel = new SimpleDataModel("12215", "25/10/2005", "JOD", new BigDecimal(1.0));
        Assertions.assertEquals("12215", simpleDataModel.getID());
        Assertions.assertEquals("25/10/2005", simpleDataModel.getDate());
        Assertions.assertEquals("JOD", simpleDataModel.getCurrencyCode());
        Assertions.assertEquals(new BigDecimal(1.0).setScale(3), simpleDataModel.getAmount());

    }

    @Test
    public void givenSimpleDatamodel_whenCallTostring_thenReturnID() {
        SimpleDataModel simpleDataModel = new SimpleDataModel("12215", "25/10/2005", "JOD", new BigDecimal(1.0));
        Assertions.assertEquals("12215",simpleDataModel.toString());

    }
    @Test
    public void givenTwoSimpleDatamodel_whenCallEquals_thenReturnTrue() {
        SimpleDataModel simpleDataModel = new SimpleDataModel("12215", "25/10/2005", "JOD", new BigDecimal(1.0));
        SimpleDataModel simpleDataModel1 = new SimpleDataModel("124785", "25/10/2005", "JOD", new BigDecimal(1.0));
        Assertions.assertEquals(simpleDataModel,simpleDataModel1);

    }

}
