import com.progressoft.reconciliation.read.models.SimpleDataModel;
import com.progressoft.reconciliation.read.readers.CSVListProvider;
import com.progressoft.reconciliation.read.readers.ReaderException;
import com.progressoft.reconciliation.read.models.TransactionDataModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class CSVReaderTest {
    @Test
    public void canCreate() throws FileNotFoundException {
        Path path = Paths.get("../sample-files/input-files/bank-transactions.csv");
        CSVListProvider CSVReader = new CSVListProvider(path);
    }


    @Test
    public void givenCSVFile_whenCallgetData_thenReturnSortedFileData() throws IOException {
        Path temp = Files.createTempFile(Paths.get("/tmp"), ".csv", ("temp"));
        try (PrintWriter printWriter = new PrintWriter(Files.newOutputStream(temp));) {
            printWriter.write("trans unique id,trans description,amount,currecny,purpose,value date,trans type\n" +
                    "TR-47884222201,online transfer,140,USD,donation,2020-01-20,D\n" +
                    "TR-47884222202,atm withdrwal,20.000,JOD,,2020-01-22,D\n" +
                    "TR-47884222203,counter withdrawal,5000,JOD,,2020-01-25,D\n");
            printWriter.flush();
        }
        CSVListProvider csvReader = new CSVListProvider(temp.toAbsolutePath());
        ArrayList<TransactionDataModel> data = csvReader.getSortedTransactionsList().getData();
        ArrayList<TransactionDataModel> sortedModels = new ArrayList<>();
        sortedModels.add(new SimpleDataModel("TR-47884222201", "2020-01-20", "USD", new BigDecimal("140")));
        sortedModels.add(new SimpleDataModel("TR-47884222202", "2020-01-22", "JOD", new BigDecimal("20")));
        sortedModels.add(new SimpleDataModel("TR-47884222203", "2020-01-25", "JOD", new BigDecimal("5000")));
        Assertions.assertEquals(sortedModels,data);
    }

    @Test
    public void givenNoAmountCSVFile_whenCallgetData_thenFail() throws IOException {
        Path temp = Files.createTempFile(Paths.get("/tmp"), ".csv", ("temp"));
        try (PrintWriter printWriter = new PrintWriter(Files.newOutputStream(temp));) {
            printWriter.write("trans unique id,trans description,currecny,purpose,value date,trans type\n" +
                    "TR-47884222205,atm withdrwal,JOD,,2020-02-02,D");
            printWriter.flush();
        }
        CSVListProvider csvReader = new CSVListProvider(temp.toAbsolutePath());
        ReaderException readerException = Assertions.assertThrows(ReaderException.class, csvReader::getSortedTransactionsList);
        Assertions.assertEquals("amount Column Not Found", readerException.getLocalizedMessage());


    }
    @Test
    public void givenNoIDFile_whenCallgetData_thenFail() throws IOException {
        Path temp = Files.createTempFile(Paths.get("/tmp"), ".csv", ("temp"));
        try (PrintWriter printWriter = new PrintWriter(Files.newOutputStream(temp));) {
            printWriter.write("trans description,currecny,purpose,value date,trans type\n" +
                    "online transfer,140,USD,donation,2020-01-20,D");
            printWriter.flush();
        }
        CSVListProvider csvReader = new CSVListProvider(temp.toAbsolutePath());
        ReaderException readerException = Assertions.assertThrows(ReaderException.class, csvReader::getSortedTransactionsList);
        Assertions.assertEquals("trans unique id Column Not Found", readerException.getLocalizedMessage());


    } @Test
    public void givenNoCurrencyCSVFile_whenCallgetData_thenFail() throws IOException {
        Path temp = Files.createTempFile(Paths.get("/tmp"), ".csv", ("temp"));
        try (PrintWriter printWriter = new PrintWriter(Files.newOutputStream(temp));) {
            printWriter.write("trans unique id,trans description,amount,purpose,value date,trans type\n" +
                    "TR-47884222205,atm withdrwal,,20.00,2020-02-02,D");
            printWriter.flush();
        }
        CSVListProvider csvReader = new CSVListProvider(temp.toAbsolutePath());
        ReaderException readerException = Assertions.assertThrows(ReaderException.class, csvReader::getSortedTransactionsList);
        Assertions.assertEquals("currecny Column Not Found", readerException.getLocalizedMessage());


    } @Test
    public void givenCorruptedCSVFile_whenCallgetData_thenFail() throws IOException {
        Path temp = Files.createTempFile(Paths.get("/tmp"), ".csv", ("temp"));
        try (PrintWriter printWriter = new PrintWriter(Files.newOutputStream(temp));) {
            printWriter.write("trans unique id,trans description,currecny,purpose,amount,trans type\n" +
                    "TR-47884222205,atm withdrwal,JOD,,,D");
            printWriter.flush();
        }
        CSVListProvider csvReader = new CSVListProvider(temp.toAbsolutePath());
        ReaderException readerException = Assertions.assertThrows(ReaderException.class, csvReader::getSortedTransactionsList);
        Assertions.assertEquals("value date Column Not Found", readerException.getLocalizedMessage());


    }
}
