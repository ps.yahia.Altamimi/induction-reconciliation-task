package com.progressoft.reconciliation.read.models;

import java.util.Comparator;

public class TransactionDataModelComperator implements Comparator<TransactionDataModel> {
    @Override
    public int compare(TransactionDataModel transactionDataModel, TransactionDataModel t1) {
        String first = transactionDataModel.getID();
        String second = t1.getID();
        return first.compareTo(second);
    }
}
