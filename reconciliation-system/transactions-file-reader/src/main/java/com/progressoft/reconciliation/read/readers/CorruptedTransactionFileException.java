package com.progressoft.reconciliation.read.readers;

public class CorruptedTransactionFileException extends RuntimeException {
    public CorruptedTransactionFileException(String s) {
        super(s);
    }
}
