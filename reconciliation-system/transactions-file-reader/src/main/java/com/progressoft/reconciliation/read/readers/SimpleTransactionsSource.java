package com.progressoft.reconciliation.read.readers;

import com.progressoft.reconciliation.read.models.TransactionDataModel;

import java.util.ArrayList;


public class SimpleTransactionsSource implements TransactionsSource {

    private ArrayList<TransactionDataModel> dataModels;

    public SimpleTransactionsSource(){
        dataModels = new ArrayList<>();
    }

    @Override
    public ArrayList<TransactionDataModel> getData() {
        return (ArrayList<TransactionDataModel>) dataModels.clone();
    }

    @Override
    public void addModel(TransactionDataModel dataModel) {
        dataModels.add(dataModel);

    }
    @Override
    public void sort() {
        dataModels.sort((t,t1)-> t.getID().compareTo(t1.getID()));
    }


}
