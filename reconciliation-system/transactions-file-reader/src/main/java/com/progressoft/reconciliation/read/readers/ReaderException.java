package com.progressoft.reconciliation.read.readers;

import java.io.IOException;

public class ReaderException extends RuntimeException {
    public ReaderException(String e) {
        super(e);
    }

    public ReaderException(String message, Exception e) {
        super(message, e);
    }
}
