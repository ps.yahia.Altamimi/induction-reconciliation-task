package com.progressoft.reconciliation.read.models;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Objects;

public class SimpleDataModel implements TransactionDataModel {
    private String ID;
    private String date;
    private String currencyCode;
    private BigDecimal amount;

    public SimpleDataModel(String ID, String date, String currencyCode, BigDecimal amount) {
        validateArgs(ID, date, currencyCode, amount);
        this.ID = ID;
        this.date = date;
        this.currencyCode = currencyCode;
        this.amount = amount;
        int defaultFractionDigits = Currency.getInstance(currencyCode).getDefaultFractionDigits();
        this.amount = amount.setScale(defaultFractionDigits, BigDecimal.ROUND_HALF_DOWN);


    }

    private void validateArgs(String ID, String date, String currencyCode, BigDecimal amount) {
        if (ID == null || ID.isEmpty())
            throw new NullPointerException("Null ID");
        if (date == null || date.isEmpty())
            throw new NullPointerException("Null Date");
        if (currencyCode == null || currencyCode.isEmpty())
            throw new NullPointerException("Null Currency Code");
        if (amount == null || amount.compareTo(BigDecimal.ZERO) < 1)
            throw new IllegalArgumentException("Non Positive Amount");


    }


    @Override
    public String getID() {
        return ID;
    }

    @Override
    public String getDate() {
        return date;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public String getCurrencyCode() {
        return currencyCode;
    }

    @Override
    public String toString() {
        return ID;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SimpleDataModel)) return false;
        SimpleDataModel that = (SimpleDataModel) o;
        return getAmount().compareTo(that.getAmount()) == 0 &&
                getDate().equals(that.getDate()) &&
                getCurrencyCode().equals(that.getCurrencyCode());
    }

 }
