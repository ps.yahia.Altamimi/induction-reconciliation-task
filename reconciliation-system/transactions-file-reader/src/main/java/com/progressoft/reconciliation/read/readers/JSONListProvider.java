package com.progressoft.reconciliation.read.readers;


import com.progressoft.reconciliation.read.models.SimpleDataModel;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Path;

public class JSONListProvider implements TransactionsListProvider {
    private Path filePath;
    private SimpleTransactionsSource dataFile;


    public JSONListProvider(Path filePath) {
        dataFile = new SimpleTransactionsSource();
        FileValidator.validatePath(filePath);
        this.filePath = filePath;
    }

    private void read() {
        try {
            JSONParser jsonParser = new JSONParser();
            JSONArray a = (JSONArray) jsonParser.parse(new FileReader(String.valueOf(filePath)));
            for (Object o : a) {
                SimpleDataModel dataModel = JSONobjectToModel((JSONObject) o);
                dataFile.addModel(dataModel);
            }
            dataFile.sort();
            FileValidator.validateDuplicates(dataFile);
        } catch (ParseException | IOException e) {
            throw new ReaderException(e.getMessage(),e);
        }
    }

    @Override
    public SimpleTransactionsSource getSortedTransactionsList() {
        if(isEmpty())
        read();
        return dataFile;
    }

    private boolean isEmpty() {
        return dataFile.getData().isEmpty();
    }

    private SimpleDataModel JSONobjectToModel(JSONObject jsonObject) {
        String ID = jsonObject.get("reference").toString();
        String date = jsonObject.get("date").toString();
        String currency = jsonObject.get("currencyCode").toString();
        String format = "dd/MM/yyyy";
        date = DateValidator.validate(date, format);
        BigDecimal amount = new BigDecimal(jsonObject.get("amount").toString());

        return new SimpleDataModel(ID, date, currency, amount);
    }


}
