package com.progressoft.reconciliation.read.readers;

public interface TransactionsListProvider {
    TransactionsSource getSortedTransactionsList();
}
