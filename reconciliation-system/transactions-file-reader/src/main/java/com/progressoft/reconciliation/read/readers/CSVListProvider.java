package com.progressoft.reconciliation.read.readers;

import com.progressoft.reconciliation.read.models.SimpleDataModel;

import java.io.*;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CSVListProvider implements TransactionsListProvider {
    private SimpleTransactionsSource dataFile;
    private Path filePath;

    public CSVListProvider(Path filePath) {
        FileValidator.validatePath(filePath);
        this.filePath = filePath;
        dataFile = new SimpleTransactionsSource();
    }


    private void read() {
        try (BufferedReader bufferedReader = Files.newBufferedReader(filePath)) {
            String firstLine = bufferedReader.readLine();
            readWithHeader(bufferedReader, firstLine);
        } catch (IOException e) {
       throw new ReaderException(e.getMessage(),e);
        }
    }

    @Override
    public TransactionsSource getSortedTransactionsList() {
        if(isEmpty())
        read();
        return dataFile;
    }

    private boolean isEmpty() {
        return dataFile.getData().isEmpty();
    }

    private void readWithHeader(BufferedReader bufferedReader, String firstLine) throws IOException {
        List<String> headers =
                Arrays.stream(firstLine.split(","))
                        .collect(Collectors.toList());
        int IDIndex = headers.indexOf("trans unique id");
        int amountIndex = headers.indexOf("amount");
        int currencyIndex = headers.indexOf("currecny");
        int dateIndex = headers.indexOf("value date");
        validateIndices(IDIndex, amountIndex, currencyIndex, dateIndex);
        addRows(bufferedReader, IDIndex, amountIndex, currencyIndex, dateIndex);
        dataFile.sort();
        FileValidator.validateDuplicates(dataFile);
    }

    private void validateIndices(int IDIndex, int amountIndex, int currencyIndex, int dateIndex) {
        if (IDIndex < 0)
            throw new ReaderException("trans unique id Column Not Found");
        if (amountIndex < 0)
            throw new ReaderException("amount Column Not Found");
        if (currencyIndex < 0)
            throw new ReaderException("currecny Column Not Found");
        if (dateIndex < 0)
            throw new ReaderException("value date Column Not Found");
    }

    private void addRows(BufferedReader bufferedReader, int IDIndex, int amountIndex, int currencyIndex, int dateIndex) throws IOException  {
        String line = "";
        while ((line = bufferedReader.readLine()) != null) {
            String[] splitted = line.split(",");
            String date = splitted[dateIndex];
            String format = "yyyy-MM-dd";
            date = DateValidator.validate(date, format);
            String ID = splitted[IDIndex];
            BigDecimal amount = new BigDecimal(splitted[amountIndex]);
            String currencyCode = splitted[currencyIndex];
            dataFile.addModel(new SimpleDataModel(ID, date, currencyCode, amount));

        }
    }


}



