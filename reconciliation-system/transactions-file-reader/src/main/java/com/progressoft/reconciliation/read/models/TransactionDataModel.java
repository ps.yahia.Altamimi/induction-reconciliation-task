package com.progressoft.reconciliation.read.models;

import java.math.BigDecimal;

public interface TransactionDataModel  {
    String getID();
    String getDate();
    BigDecimal getAmount();
    String getCurrencyCode();
}
