package com.progressoft.reconciliation.read.readers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateValidator {
    public static String validate(String date, String format) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(format);
            Date parsedDate = formatter.parse(date);
            if (!format.equals("yyyy-MM-dd")) {
                SimpleDateFormat unifier = new SimpleDateFormat("yyyy-MM-dd");
                String unified = unifier.format(parsedDate);
                return unified;
            }
            return date;
        } catch (ParseException e) {
            throw new DateFormatException(e.getMessage() + " With Format " + format,e);
        }
    }
}
