package com.progressoft.reconciliation.read.readers;

import java.text.ParseException;

public class DateFormatException extends RuntimeException {
    public DateFormatException(String message, ParseException e) {
        super(message,e);
    }
}
