package com.progressoft.reconciliation.read.readers;


import com.progressoft.reconciliation.read.models.TransactionDataModel;

import java.util.ArrayList;

public interface TransactionsSource {
    ArrayList<TransactionDataModel> getData();
    void addModel(TransactionDataModel dataModel);
    void sort();


}
