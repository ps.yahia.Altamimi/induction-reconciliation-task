package com.progressoft.app;

import com.progressoft.reconciliation.compare.CsvFilesReportGenerator;
import com.progressoft.reconciliation.compare.TransactionsComparer;
import com.progressoft.reconciliation.compare.FileDetails;
import com.progressoft.reconciliation.compare.TransactionsListFactory;
import com.progressoft.reconciliation.read.readers.TransactionsSource;

import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws ParseException {
        Scanner input = new Scanner(System.in);
        System.out.println(">> Enter source file location:");
        String sourcePath = input.nextLine();
        System.out.println(">> Enter source file format:");
        String sourceFormat = input.nextLine();
        System.out.println(">> Enter target file location:");
        String targetPath = input.nextLine();
        System.out.println(">> Enter target file format:");
        String targetFormat = input.nextLine();
        doCompare(sourcePath, sourceFormat, targetPath, targetFormat);


    }

    private static void doCompare(String sourcePath, String sourceFormat, String targetPath, String targetFormat) {
        FileDetails sourceDetails = new FileDetails(Paths.get(sourcePath), sourceFormat);
        FileDetails targetDetails = new FileDetails(Paths.get(targetPath), targetFormat);

        TransactionsSource sourceFile = TransactionsListFactory.newTransactionsList(sourceDetails);
        TransactionsSource targetFile = TransactionsListFactory.newTransactionsList(targetDetails);
        TransactionsComparer transactionsComparer = new TransactionsComparer(new CsvFilesReportGenerator(Paths.get("./target/result-files")));
        transactionsComparer.compare(sourceFile, targetFile);
        URI resultDirectoryUri = Paths.get("./target/result-files").toUri();
        System.out.println("Reconciliation finished.\n" +
                "Result files are availble in directory " + resultDirectoryUri);
    }
}
