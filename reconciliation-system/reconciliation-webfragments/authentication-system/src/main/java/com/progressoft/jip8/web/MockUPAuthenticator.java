package com.progressoft.jip8.web;

import java.util.Objects;

public class MockUPAuthenticator implements Authenticator<Object> {
    @Override
    public UserDetails authenticate(Object... tokens) {
        Objects.requireNonNull(tokens,"Null Tokens Args");
        if(tokens.length!=2)
            throw new InvalidTokensNumber();
        Objects.requireNonNull(tokens[0],"Null Token Value");
        Objects.requireNonNull(tokens[1],"Null Token Value");
      if(!checkValues(tokens)){
          return null;
      }
      else return new MockUserDetails();
    }

    private boolean checkValues(Object[] tokens) {
        return tokens[0].toString().equals("admin")&&tokens[1].toString().equals("admin");
    }

    private class MockUserDetails implements UserDetails {
        @Override
        public String getUserName() {
            return "admin";
        }

        @Override
        public String getUserID() {
            return "admin";
        }
    }
}
