package com.progressoft.jip8.web;

public interface UserDetails {
    String getUserName();
    String getUserID();
}
