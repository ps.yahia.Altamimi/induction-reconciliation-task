package com.progressoft.jip8.web;

import com.progressoft.reconciliation.read.readers.ReaderException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class RenderServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if (!Objects.nonNull(session.getAttribute("resultDirectory"))) {
            resp.sendRedirect(req.getContextPath() + "/upload");
            return;
        }
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/WEB-INF/result.jsp");
        try {
            requestDispatcher.forward(req, resp);
        } catch (ServletException e) {
            throw new RenderException("Unable To render result ", e);
        } catch (IOException e) {
            throw new ReaderException(e.getMessage(), e);
        }

    }
}
