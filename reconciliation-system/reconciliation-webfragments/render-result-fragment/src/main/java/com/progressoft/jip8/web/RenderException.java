package com.progressoft.jip8.web;

import javax.servlet.ServletException;

public class RenderException extends RuntimeException {
    public RenderException(String s, ServletException e) {
        super(s,e);
    }
}
