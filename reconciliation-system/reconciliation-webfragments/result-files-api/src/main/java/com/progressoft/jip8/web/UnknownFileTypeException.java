package com.progressoft.jip8.web;

public class UnknownFileTypeException extends RuntimeException {
    public UnknownFileTypeException(String s) {
        super(s);
    }
}
