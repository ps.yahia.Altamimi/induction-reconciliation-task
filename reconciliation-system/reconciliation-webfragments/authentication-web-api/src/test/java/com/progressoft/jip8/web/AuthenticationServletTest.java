package com.progressoft.jip8.web;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

public class AuthenticationServletTest {

    @Test
    public void givenInvalidUsernameAndPassword_whenDoPost_thenPrintFailed() throws IOException, ServletException {

        HttpServletRequest request= Mockito.mock(HttpServletRequest.class);
        HttpServletResponse response=Mockito.mock(HttpServletResponse.class);
        HttpSession session=Mockito.mock(HttpSession.class);
        Enumeration<String> enumeration=Mockito.mock(Enumeration.class);
        Authenticator authenticator=Mockito.mock(Authenticator.class);
        PrintWriter writer=Mockito.mock(PrintWriter.class);
        AuthenticateServlet servlet=new AuthenticateServlet(authenticator);
        Mockito.when(request.getSession()).thenReturn(session);
        Mockito.when(request.getParameterNames()).thenReturn(enumeration);
        Mockito.when(response.getWriter()).thenReturn(writer);
        servlet.doPost(request,response);
        Mockito.verify(response.getWriter()).print("failed");
    }

    @Test
    public void givenValidUsernameAndPassword_whenDoPost_thenPrintSucces() throws IOException, ServletException {

        HttpServletRequest request= Mockito.mock(HttpServletRequest.class);
        HttpServletResponse response=Mockito.mock(HttpServletResponse.class);
        HttpSession session=Mockito.mock(HttpSession.class);
        Enumeration<String> enumeration=new Enumeration<String>() {
            String [] names=new String[]{"username","password"};
            int pointer=0;
            @Override
            public boolean hasMoreElements() {
                return pointer< names.length;
            }

            @Override
            public String nextElement() {
                String data=names[pointer];
                pointer++;
                return data;
            }
        };
        PrintWriter writer=Mockito.mock(PrintWriter.class);
        AuthenticateServlet servlet=new AuthenticateServlet(new MockUPAuthenticator());
        Mockito.when(request.getSession()).thenReturn(session);
        Mockito.when(request.getParameterNames()).thenReturn(enumeration);
        Mockito.when(response.getWriter()).thenReturn(writer);
        Mockito.when(request.getParameter("username")).thenReturn("admin");
        Mockito.when(request.getParameter("password")).thenReturn("admin");
        servlet.doPost(request,response);
        Mockito.verify(response.getWriter()).print("success");
    }

}