package com.progressoft.jip8.web;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;

public class AuthenticateServlet extends HttpServlet {
    private Authenticator authenticator;

    public AuthenticateServlet(Authenticator authenticator) {
        this.authenticator = authenticator;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ArrayList<String> parameters = new ArrayList<>();
        Enumeration<String> parameterNames = req.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String paramValue = req.getParameter(parameterNames.nextElement());
            parameters.add(paramValue);
        }
        UserDetails userDetails = authenticator.authenticate(parameters.toArray());
        if (userDetails!=null) {
            // TODO enhance (not always admin) #DONE
            req.getSession().setAttribute("userName", userDetails.getUserName());
            resp.getWriter().print("success");
            return;
        }
        resp.getWriter().print("failed");
    }
}

