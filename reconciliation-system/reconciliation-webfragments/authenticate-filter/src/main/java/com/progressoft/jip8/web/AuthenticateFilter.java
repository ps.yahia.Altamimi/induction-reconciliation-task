package com.progressoft.jip8.web;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;
import java.util.logging.Logger;

public class AuthenticateFilter implements Filter {
    public AuthenticateFilter() {
        Logger logger = Logger.getLogger(AuthenticateFilter.class.getName());
        logger.warning("filter entered");
    }

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse res = (HttpServletResponse) servletResponse;
        String username = (String) req.getSession().getAttribute("userName");
        if (username == null || !username.equalsIgnoreCase("admin")) {
            res.sendRedirect(req.getContextPath() + "/");
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);

    }

    @Override
    public void destroy() {

    }
}

