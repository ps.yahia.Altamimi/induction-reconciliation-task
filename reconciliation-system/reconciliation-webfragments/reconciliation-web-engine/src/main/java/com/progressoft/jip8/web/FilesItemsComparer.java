package com.progressoft.jip8.web;

import com.progressoft.jip8.FilesException;
import com.progressoft.reconciliation.compare.*;
import com.progressoft.reconciliation.read.readers.TransactionsSource;
import org.apache.commons.fileupload.FileItem;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

public class FilesItemsComparer implements FilesComparer<FileItem>{
    // TODO pass file items through this method #DONE
    public void doCompare(Path uploadDirectory,List<FileItem> fileItems) {
        try {
            validateParams(uploadDirectory, fileItems);
            FileDetails sourceDetails = getFileDetails(fileItems,uploadDirectory, "source", 0, 2, 1);
            FileDetails targetDetails = getFileDetails(fileItems,uploadDirectory, "target", 3, 5, 4);
            ReportGenerator reportGenerator;
            String type = getString(fileItems, 6);
            if (type.equalsIgnoreCase("CSV")) {
                reportGenerator = new CsvFilesReportGenerator(uploadDirectory);
            } else if (type.equalsIgnoreCase("JSON")) {
                reportGenerator = new JsonFilesReportGenerator(uploadDirectory);
            } else {
                throw new UnknownFileTypeException(type +"is not a supported file extension");
            }
            compare(sourceDetails, targetDetails, reportGenerator);
        } catch (Exception e) {
            // TODO you should throw not return false, swallowing #DONE
            throw new FilesException(e.getMessage(),e);
        }
    }

    private void validateParams(Path uploadDirectory, List<FileItem> fileItems) {
        Objects.requireNonNull(fileItems, "Null File Items");
        Objects.requireNonNull(uploadDirectory, "Null Upload Directory");
    }

    private FileDetails getFileDetails(List<FileItem> fileItems,Path uploadDirectory, String prefix, int i, int i2, int i3) {
        String sourceName = prefix + getString(fileItems, i);
        String sourceExtension = getString(fileItems, i2);
        File sourceFile = new File(uploadDirectory + File.separator + sourceName + "." + sourceExtension);
        try {
            fileItems.get(i3).write(sourceFile);
        } catch (Exception e) {
            throw new FilesException("can't write file ", e);
        }
        return new FileDetails(sourceFile.toPath(), sourceExtension);
    }

    private void compare(FileDetails sourceDetails, FileDetails targetDetails, ReportGenerator reportGenerator) {

        TransactionsSource transactionsSource = TransactionsListFactory.newTransactionsList(sourceDetails);
        TransactionsSource transactionsTarget = TransactionsListFactory.newTransactionsList(targetDetails);
        TransactionsComparer transactionsComparer = new TransactionsComparer(reportGenerator);
        transactionsComparer.compare(transactionsSource, transactionsTarget);
    }

    public static String getString(List<FileItem> items, int index) {
        try {
            String result = "";
            BufferedReader inputStream = new BufferedReader(new InputStreamReader(items.get(index).getInputStream()));

            String s;
            while ((s = inputStream.readLine()) != null) {
                result += s;

            }
            return result;
        } catch (IOException e) {
            throw new FilesException("failed to upload some files", e);
        }

    }
}
