package com.progressoft.jip8.web;

import java.nio.file.Path;
import java.util.List;

public interface FilesComparer<T> {
     void doCompare(Path path,List<T> list);
}
