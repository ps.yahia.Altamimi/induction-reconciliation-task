package com.progressoft.jip8.web;

import com.progressoft.jip8.FilesException;
import com.progressoft.reconciliation.compare.CsvFilesReportGenerator;
import com.progressoft.reconciliation.compare.FileDetails;
import com.progressoft.reconciliation.compare.TransactionsComparer;
import com.progressoft.reconciliation.compare.TransactionsListFactory;
import com.progressoft.reconciliation.read.readers.ReaderException;
import com.progressoft.reconciliation.read.readers.TransactionsSource;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class CompareServlet extends HttpServlet {
    private FilesComparer filesComparer;

    public CompareServlet(FilesComparer filesComparer) {
        this.filesComparer = filesComparer;
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.changeSessionId();
        FileItemFactory itemFactory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(itemFactory);
        upload(req, resp, upload);
    }

    private void upload(HttpServletRequest req, HttpServletResponse resp, ServletFileUpload upload) throws IOException {
        try {
            List<FileItem> items = upload.parseRequest(req);
            Path uploadDirectory = Files.createTempDirectory("tmp");
            // TODO after passing items to the method, you can have FilesItemsComparer as a dependency #DONE
            filesComparer.doCompare(uploadDirectory, items);
            req.getSession().setAttribute("resultDirectory", uploadDirectory);
            req.getSession().setAttribute("resultType", FilesItemsComparer.getString(items, 6));
            resp.sendRedirect("result");
        } catch (FileUploadException e) {
            throw new FilesException("can't parse request ", e);
        }
    }
}
