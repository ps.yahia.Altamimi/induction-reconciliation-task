package com.progressoft.reconciliation.compare;

import com.progressoft.reconciliation.read.readers.FileValidator;

import java.nio.file.Path;

public class FileDetails {
    private Path path;
    private String extension;

    public FileDetails(Path path, String extension) {
        FileValidator.validatePath(path);
        this.path = path;
        this.extension = extension;
    }

    public Path getPath() {
        return path;
    }

    public String getExtension() {
        return extension;
    }


}
