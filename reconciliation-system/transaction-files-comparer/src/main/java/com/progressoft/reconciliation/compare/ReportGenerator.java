package com.progressoft.reconciliation.compare;

import com.progressoft.reconciliation.read.models.TransactionDataModel;

public interface ReportGenerator<T>  extends AutoCloseable{
    void writeToMatchingFile(TransactionDataModel model);

    void writeToMismatchingFile(TransactionDataModel model,String Type);

    void writeToMissingFile(TransactionDataModel model,String type);

    T format(TransactionDataModel transactionDataModel);

}
