package com.progressoft.reconciliation.compare;

public class WriterException extends RuntimeException {
    public WriterException(String s, Exception e) {
        super(s,e);
    }
}
