package com.progressoft.reconciliation.compare;

import com.progressoft.reconciliation.read.models.TransactionDataModel;
import com.progressoft.reconciliation.read.models.TransactionDataModelComperator;
import com.progressoft.reconciliation.read.readers.TransactionsSource;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;

public class TransactionsComparer {
private ReportGenerator reportGenerator;
public TransactionsComparer(ReportGenerator reportGenerator){
    this.reportGenerator=reportGenerator;
}
    public void compare(TransactionsSource... lists) {
        TransactionsSource source=lists[0];
        TransactionsSource target=lists[1];
        try (ReportGenerator filesReportGenerator=reportGenerator) {
            ArrayList<TransactionDataModel> sourceData = source.getData();
            ArrayList<TransactionDataModel> targetData = target.getData();
            for (TransactionDataModel sourceModel : sourceData) {
                int matchIndex = findMatch(targetData, sourceModel);
                writeToSuitedFile(filesReportGenerator, targetData, sourceModel, matchIndex);
            }
            for (TransactionDataModel targetModel : targetData) {
                filesReportGenerator.writeToMissingFile(targetModel, "TARGET,");
            }

        } catch (Exception e) {
            throw new WriterException("Writing Went wrong",e);
        }
    }


    private static void writeToSuitedFile(ReportGenerator filesReportGenerator, ArrayList<TransactionDataModel> targetData, TransactionDataModel sourceModel, int matchIndex) {
        if (matchIndex < 0) {
            filesReportGenerator.writeToMissingFile(sourceModel,"SOURCE," );
            return;
        }
        TransactionDataModel targetModel = targetData.remove(matchIndex);

        compareThenWrite(filesReportGenerator, sourceModel, targetModel);
    }

    private static void compareThenWrite(ReportGenerator filesReportGenerator, TransactionDataModel sourceModel,TransactionDataModel targetModel) {
        if (sourceModel.equals(targetModel)) {
            filesReportGenerator.writeToMatchingFile(sourceModel);
            return;
        }
       filesReportGenerator.writeToMismatchingFile( sourceModel,"SOURCE,");
        filesReportGenerator.writeToMismatchingFile( targetModel,"TARGET,");
    }



    private static int findMatch(ArrayList<TransactionDataModel> targetData, TransactionDataModel sourceModel) {
        return Collections.binarySearch(targetData, sourceModel, new TransactionDataModelComperator());
    }
}



