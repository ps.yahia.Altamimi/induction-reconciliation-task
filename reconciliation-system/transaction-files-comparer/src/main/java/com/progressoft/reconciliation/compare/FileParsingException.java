package com.progressoft.reconciliation.compare;

public class FileParsingException extends RuntimeException {
    public FileParsingException(String s) {
        super(s);
    }
}
