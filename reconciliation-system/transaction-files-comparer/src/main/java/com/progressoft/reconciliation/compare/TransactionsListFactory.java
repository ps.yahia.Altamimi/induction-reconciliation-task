package com.progressoft.reconciliation.compare;


import com.progressoft.reconciliation.read.readers.*;

import java.nio.file.Path;

public class TransactionsListFactory {

    public static TransactionsSource newTransactionsList(FileDetails fileDetails) {

        TransactionsListProvider reader = getTransactionsListProvider(fileDetails);
        return reader.getSortedTransactionsList();
    }

    private static TransactionsListProvider getTransactionsListProvider(FileDetails fileDetails) {
        String extension = fileDetails.getExtension();
        Path path = fileDetails.getPath();
        if (extension.equalsIgnoreCase("CSV")) {
            return new CSVListProvider(path);
        } else if (extension.equalsIgnoreCase("JSON")) {
            return new JSONListProvider(path);
        } else throw new FileParsingException("Not Supported File Extension");
    }

}
