import com.google.gson.*;
import com.progressoft.reconciliation.compare.JsonFilesReportGenerator;
import com.progressoft.reconciliation.read.models.SimpleDataModel;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class JsonFilesReportGeneratorTest {
    @Test
    public void givenTransactionModel_whenFormat_thenReturnJsonObject() {
        SimpleDataModel simpleDataModel = new SimpleDataModel("Tr-12454", "2020-01-31", "JOD", BigDecimal.ONE);
        JsonFilesReportGenerator jsonFilesReportGenerator = new JsonFilesReportGenerator(Paths.get("./target"));
        String expectedResult = "{" +
                "\"ID\":\"Tr-12454\"," +
                "\"date\":\"2020-01-31\"," +
                "\"currencyCode\":\"JOD\"," +
                "\"amount\":1.000" +
                "}";
        JsonObject format = jsonFilesReportGenerator.format(simpleDataModel);
        Assertions.assertEquals(expectedResult, format.toString());
    }

    @Test
    public void givenTransactionModel_whenWrite_thenReturnCorrectFiles() throws IOException {
        SimpleDataModel simpleDataModel = new SimpleDataModel("Tr-12454", "2020-01-31", "JOD", BigDecimal.ONE);
        Path resultPath = Paths.get("./target/result-files/");
        JsonFilesReportGenerator jsonFilesReportGenerator = new JsonFilesReportGenerator(resultPath);
        jsonFilesReportGenerator.writeToMatchingFile(simpleDataModel);
        jsonFilesReportGenerator.writeToMismatchingFile(simpleDataModel, "Target,");
        jsonFilesReportGenerator.writeToMissingFile(simpleDataModel, "Target,");
        jsonFilesReportGenerator.close();
        File actualMatchingFile = Paths.get("./target/result-files/Matching-Transactions-file.json").toFile();
        File actualMismatchingFile = Paths.get("./target/result-files/Mismatching-Transactions-file.json").toFile();
        File actualMissingFile = Paths.get("./target/result-files/Missing-Transactions-file.json").toFile();
        Path [] paths=prepareExpectedFiles();
        Assertions.assertTrue(FileUtils.contentEquals(paths[0].toFile(),actualMatchingFile));
        Assertions.assertTrue(FileUtils.contentEquals(paths[1].toFile(),actualMismatchingFile));
        Assertions.assertTrue(FileUtils.contentEquals(paths[1].toFile(),actualMissingFile));



    }

    private Path[] prepareExpectedFiles() throws IOException {
        Path file = Files.createTempFile("tmp1", "json");
        Path file1 = Files.createTempFile("tmp1", "json");
        PrintWriter printWriter=new PrintWriter(Files.newOutputStream(file));
        printWriter.write("[{\"ID\":\"Tr-12454\",\"date\":\"2020-01-31\",\"currencyCode\":\"JOD\",\"amount\":1.000}]");
        printWriter.flush();
        printWriter=new PrintWriter(Files.newOutputStream(file1));
        printWriter.write("[{\"Found in file\":\"Target\",\"ID\":\"Tr-12454\",\"date\":\"2020-01-31\",\"currencyCode\":\"JOD\",\"amount\":1.000}]");
        printWriter.flush();
        printWriter.close();
        return new Path[]{file,file1};
    }
}
