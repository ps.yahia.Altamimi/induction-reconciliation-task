import com.progressoft.reconciliation.compare.CsvFilesReportGenerator;
import com.progressoft.reconciliation.read.readers.TransactionsSource;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import com.progressoft.reconciliation.compare.TransactionsComparer;
import com.progressoft.reconciliation.compare.FileDetails;
import com.progressoft.reconciliation.compare.TransactionsListFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TransactionsComparerTest {
    @Test
    public void givenInputFiles_whenCompare_thenProduceCorrectResultFiles() throws IOException {
        Path source = Paths.get("../sample-files/input-files/bank-transactions.csv");
        Path target = Paths.get("../sample-files/input-files/online-banking-transactions.json");
        TransactionsListFactory transactionsListFactory = new TransactionsListFactory();
        TransactionsSource sourceFile = transactionsListFactory.newTransactionsList(new FileDetails(source, "CSV"));
        TransactionsSource targetFile = transactionsListFactory.newTransactionsList(new FileDetails(target, "JSON"));
        TransactionsComparer transactionsComparer = new TransactionsComparer(new CsvFilesReportGenerator(Paths.get("./target/result-files/")));
        transactionsComparer.compare(sourceFile, targetFile);
        File expectedMatchingFile =  Paths.get(".","src","test","resources","sample-files","result-files","matching-transactions.csv").toFile();
        File actualMatchingFile = Paths.get("./target/result-files/Matching-Transactions-file.csv").toFile();
        File expectedMismatchingFile = Paths.get(".","src","test","resources","sample-files","result-files","mismatched-transactions.csv").toFile();
        File actualMismatchingFile = Paths.get("./target/result-files/Mismatching-Transactions-file.csv").toFile();
        File expectedMissingFile = Paths.get(".","src","test","resources","sample-files","result-files","missing-transactions.csv").toFile();
        File actualMissingFile = Paths.get("./target/result-files/Missing-Transactions-file.csv").toFile();   Assertions.assertTrue(FileUtils.contentEquals(expectedMatchingFile, actualMatchingFile));
        Assertions.assertTrue(FileUtils.contentEquals(expectedMatchingFile, actualMatchingFile));
        Assertions.assertTrue(FileUtils.contentEquals(expectedMismatchingFile, actualMismatchingFile));
        Assertions.assertTrue(FileUtils.contentEquals(expectedMissingFile, actualMissingFile));
    }
}
