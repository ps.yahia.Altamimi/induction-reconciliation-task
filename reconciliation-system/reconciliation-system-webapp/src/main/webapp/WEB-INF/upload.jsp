<%@taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html lang="en" id="basecontainer">
<head>
    <title>Compare</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="${pageContext.request.contextPath}/jquery/jquery-3.4.1.js"></script>
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!--===============================================================================================-->
    <script>
        $(document).ready(function () {
            $('#targetForm').hide()
            $('#Summary').hide()

        })

        function showTarget() {
            let sname = $('#SourceFileName').val()!="";
            let sFile = document.getElementById("SourceFile").files.length > 0 ;

            if (sname && sFile) {
                $('#Summary').hide()
                $("#sourceForm").hide()
                $("#targetForm").show()
            } else alert("Fill all fields")

        }

        function showSource() {
            $('#sourceForm').show()
            $('#targetForm').hide()
        }

        function goToSummary() {
            let sname = $('#TargetFileName').val()!="";
            let sFile = document.getElementById("TargetFile").files.length > 0 ;
            if (sname && sFile) {
                fillSummary()
                $('#sourceForm').hide()
                $('#targetForm').hide()
                $("#Summary").show()

            } else alert("Fill all the fields")
        }

        function resetForm() {
            location.replace("upload")

        }

        function fillSummary() {
            $('#summaryTargetName').text("Name :" + $('#TargetFileName').val())
            $('#summarySourceName').text("Name :" + $('#SourceFileName').val())
            $('#summaryTargetType').text("Type :" + $('#TargetFileType').val())
            $('#summarySourceType').text("Type :" + $('#SourceFileType').val())

        }



    </script>
</head>
<body>
<form action="${pageContext.request.contextPath}/compare" method="post" enctype="multipart/form-data" class="container-login100" style="background-image: url('images/bg-01.jpg');">

    <div style="width: 700px" class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
        <div class="login100-form validate-form">

            <tags:upload-file previousOnclick="" uploadOnclick="showTarget()" tagId="sourceForm" type="Source"
                              previousShown="hidden"></tags:upload-file>
            <tags:upload-file uploadOnclick="goToSummary()" previousOnclick="showSource()" tagId="targetForm"
                              type="Target" previousShown=""></tags:upload-file>
        </div>
        <div class="card-deck" id="Summary">
            <div class="card ">
                <div class="card-body text-center">
                    <h3 class="card-text">Source</h3>
                    <hr style="background-color: #007bff">
                    <h4 style="text-align: left" id="summarySourceName">Name :</h4>
                    <h4 style="text-align: left" id="summarySourceType">Type :</h4>

                </div>
            </div>

            <div class="card ">
                <div class="card-body text-center">
                    <h3 class="card-text" style="border-color: #007bff;border-width: thick">Target</h3>
                    <hr style="background-color: #007bff">
                    <h4 style="text-align: left" id="summaryTargetName">Name :</h4>
                    <h4 style="text-align: left" id="summaryTargetType">Type :</h4>
                </div>
            </div>
<div style="margin-top: 20px;" class="container-login100-form-btn">
            <label for="resultType"class="col-6">
                <h4>Result Files Format :</h4></label>
    <br>
            <select  class="col-8" style="display: block" id="resultType" name="resultType">
                <option value="csv">CSV</option>
                <option value="json">JSON</option>
            </select>
</div>
            <div style="display: -webkit-inline-flex;
margin-top: 25px" class="container-login100-form-btn">
                <button type="button" onclick="resetForm()" style="background-color: #dfb3b7;color: #1b1e21"  class="login99-form-btn">
                    Cancel
                </button>
                <button  type="button" style="color:#1b1e21;background-color: whitesmoke;margin-left: 40px;border-width: medium"
                        onclick="showTarget()" class="login100-form-btn">
                    Back
                </button>
                <button type="submit"  class="login100-form-btn">
                    Compare
                </button>


            </div>
        </div>

    </div>
</form>

</div>


<!--===============================================================================================-->
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/bootstrap/js/popper.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/daterangepicker/moment.min.js"></script>
<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="js/main.js"></script>

</body>
</html>